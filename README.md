# Frontend
A simple frontend.  
Just a nginx container that returns 200 with message `Frontend: Hello from ${NAMESPACE}`.

This is done by using `envsubst` during docker build to replace [nginx.conf.template](./nginx.conf.template)'s `
${NAMESPACE}` with an env variable.

[source](https://stackoverflow.com/questions/21866477/nginx-use-environment-variables)

## To run
```commandline
docker build -t frontend .
docker run --rm -it --name frontend -p 8080:80 -e NAMESPACE="frontend" frontend

# poking it
curl http://127.0.0.1:8080
```
